Rails.application.routes.draw do
  get "up" => "rails/health#show", as: :rails_health_check

  namespace :v1 do
    post "/auth/login", to: "auth#login"

    post "/wallets/deposit", to: "wallets#deposit"
    post "/wallets/withdraw", to: "wallets#withdraw"
    post "/wallets/transfer", to: "wallets#transfer"
    get "/wallets/balance", to: "wallets#balance"
  end
end

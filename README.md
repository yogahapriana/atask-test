# README

## How to Run
### Add environment variable
- Rename `.env.example` to `.env`, put your rapid api key in `RAPID_API_KEY` 

### Run the apps
```
$ rails db:create // Create database
$ rails db:migrate // Migrate database
$ rails s // Run rails server
```

## Curl Sample

### Login
#### Request
```
curl --location 'http://localhost:3000/v1/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "thenelse.v2@gmail.com",
    "password": "123456"
}'
```

#### Response
```
{
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2OTkwMTc5NjJ9.fjyncr582R2la_7yhIFFmh0TmY-a8qsOnixhXpv1NYY",
    "exp": "11-03-2023 20:26",
    "username": "thenelse.v2@gmail.com"
}
```

### Deposit
#### Request
```
curl --location 'http://localhost:3000/v1/wallets/deposit' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2OTkwMTc5NjJ9.fjyncr582R2la_7yhIFFmh0TmY-a8qsOnixhXpv1NYY' \
--header 'Content-Type: application/json' \
--data '{
    "wallet_id": 1,
    "credit_amount": 10000,
    "source_payment": "bank"
}'
```
#### Response
```
{
    "data": {
        "id": 48,
        "balance_amount": 100000.0,
        "credit_amount": 10000.0,
        "debit_amount": 0.0,
        "wallet_id": 1,
        "source_payment": "bank",
        "created_at": "2023-11-03T06:05:08.085Z",
        "updated_at": "2023-11-03T06:05:08.085Z",
        "target_payment": null,
        "transaction_type": "deposit",
        "source_wallet_id": null,
        "target_wallet_id": null
    }
}
```

### Withdraw
#### Request
```
curl --location 'http://localhost:3000/v1/wallets/withdraw' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2OTkwMTc5NjJ9.fjyncr582R2la_7yhIFFmh0TmY-a8qsOnixhXpv1NYY' \
--header 'Content-Type: application/json' \
--data '{
    "wallet_id": 1,
    "debit_amount": 10000,
    "target_payment": "bank"
}'
```
#### Response
```
{
    "data": {
        "id": 49,
        "balance_amount": 90000.0,
        "credit_amount": 0.0,
        "debit_amount": 10000.0,
        "wallet_id": 1,
        "source_payment": null,
        "created_at": "2023-11-03T06:05:11.142Z",
        "updated_at": "2023-11-03T06:05:11.142Z",
        "target_payment": "bank",
        "transaction_type": "withdraw",
        "source_wallet_id": null,
        "target_wallet_id": null
    }
}
```

### Transfer
#### Request
```
curl --location 'http://localhost:3000/v1/wallets/transfer' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2OTkwMTc5NjJ9.fjyncr582R2la_7yhIFFmh0TmY-a8qsOnixhXpv1NYY' \
--header 'Content-Type: application/json' \
--data '{
    "wallet_id": 1,
    "transfer_amount": 10000,
    "target_wallet_id": 2
}'
```

#### Response
```
{
    "data": {
        "id": 50,
        "balance_amount": 80000.0,
        "credit_amount": 0.0,
        "debit_amount": 10000.0,
        "wallet_id": 1,
        "source_payment": null,
        "created_at": "2023-11-03T06:05:14.229Z",
        "updated_at": "2023-11-03T06:05:14.229Z",
        "target_payment": null,
        "transaction_type": "transfer",
        "source_wallet_id": null,
        "target_wallet_id": 2
    }
}
```

### Balance
#### Request
```
curl --location --request GET 'http://localhost:3000/v1/wallets/balance' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2OTkwMTc5NjJ9.fjyncr582R2la_7yhIFFmh0TmY-a8qsOnixhXpv1NYY' \
--header 'Content-Type: application/json' \
--data '{
    "wallet_id": 1,
    "transfer_amount": 10000,
    "target_wallet_id": 2
}'
```

#### Response
```
{
    "data": {
        "my_wallets": [
            {
                "id": 1,
                "name": "Wallet 1",
                "balance_amount": 80000.0
            }
        ],
        "team_wallets": [
            {
                "id": 3,
                "name": "Wallet 3",
                "balance_amount": 0.0
            },
            {
                "id": 4,
                "name": "Wallet 3",
                "balance_amount": 0.0
            }
        ]
    }
}
```
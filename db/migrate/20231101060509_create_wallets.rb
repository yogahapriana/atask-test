class CreateWallets < ActiveRecord::Migration[7.1]
  def change
    create_table :wallets do |t|
      t.string :name, null: false
      t.integer :owner_id, null: false
      t.string :owner_type, null: false

      t.timestamps
    end
  end
end

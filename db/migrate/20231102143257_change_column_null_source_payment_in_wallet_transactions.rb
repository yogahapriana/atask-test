class ChangeColumnNullSourcePaymentInWalletTransactions < ActiveRecord::Migration[7.1]
  def change
    change_column_null :wallet_transactions, :source_payment, true
  end
end

class AddTransactionTypeToWalletTransactions < ActiveRecord::Migration[7.1]
  def change
    add_column :wallet_transactions, :transaction_type, :string, null: false
  end
end

class RenameColumnSourceInWalletTransactions < ActiveRecord::Migration[7.1]
  def change
    rename_column :wallet_transactions, :source, :source_payment
  end
end

class CreateWalletTransactions < ActiveRecord::Migration[7.1]
  def change
    create_table :wallet_transactions do |t|
      t.float :balance_amount, null: false, default: 0
      t.float :credit_amount, null: false, default: 0
      t.float :debit_amount, null: false, default: 0
      t.references :wallet, null: false, foreign_key: true
      t.string :source, null: false

      t.timestamps
    end
  end
end

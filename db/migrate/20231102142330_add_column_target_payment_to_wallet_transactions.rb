class AddColumnTargetPaymentToWalletTransactions < ActiveRecord::Migration[7.1]
  def change
    add_column :wallet_transactions, :target_payment, :string
  end
end

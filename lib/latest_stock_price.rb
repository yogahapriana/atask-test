# frozen_string_literal: true
require 'uri'
require 'net/http'

class LatestStockPrice
  HOST = "latest-stock-price.p.rapidapi.com"

  def self.common_outbound(url)
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    request = Net::HTTP::Get.new(url)
    request["X-RapidAPI-Key"] = ENV['RAPID_API_KEY']
    request["X-RapidAPI-Host"] = HOST

    response = http.request(request)
    response_body = JSON.parse(response.read_body)
    result = response_body.map do |rb|
      LatestStockPrice::StockPrice.new(rb.transform_keys(&:underscore))
    end

    result
  end

  def self.price_all(identifier = nil)
    common_outbound(URI("https://#{HOST}/any?Identifier=#{identifier}"))
  end

  def self.prices(indices, identifier = nil)
    common_outbound(URI("https://#{HOST}/price?Indices=#{indices}&Identifier=#{identifier}"))
  end
end
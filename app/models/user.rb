class User < ApplicationRecord
  has_secure_password

  has_and_belongs_to_many :teams, join_table: 'user_teams'
  has_many :wallets, foreign_key: 'owner_id', as: :owner
  has_many :wallet_transactions, through: :wallets, source: :wallet_transactions

  validates :username, presence: true, uniqueness: true
  validates :password, length: { minimum: 6 }, if: -> { new_record? || !password.nil? }

  def construct_wallet_balance(wallet)
    {
      id: wallet.id,
      name: wallet.name,
      balance_amount: WalletTransaction.last_balance_amount(wallet)
    }
  end

  def my_wallets
    result = []
    self.wallets.each do |wallet|
      result << construct_wallet_balance(wallet)
    end
    result
  end

  def team_wallets
    result = []
    self.teams.each do |team|
      team.wallets.each do |wallet|
        result << construct_wallet_balance(wallet)
      end
    end
    result
  end
end

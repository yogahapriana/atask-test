class WalletTransaction < ApplicationRecord
  belongs_to :wallet
  belongs_to :target_wallet, class_name: 'Wallet', foreign_key: 'target_wallet_id', optional: true

  validates :balance_amount, presence: true
  validates :source_payment, presence: true, if: :is_deposit?
  validates :target_payment, presence: true, if: :is_withdraw?
  validates :transfer_amount, presence: true, if: :is_transfer?
  validates :credit_amount, numericality: { greater_than_or_equal_to: 10_000, less_than_or_equal_to: 250_000_000 }, if: :is_deposit?
  validates :debit_amount, numericality: { greater_than_or_equal_to: 10_000, less_than_or_equal_to: 250_000_000 }, if: :is_withdraw?

  validate :wallet_ownership, unless: :skip_validation?
  validate :validate_transfer, if: :is_transfer?

  attr_accessor :current_user, :is_deposit, :is_withdraw, :is_transfer, :transfer_amount, :skip_validation

  def deposit
    self.target_payment = nil
    self.transaction_type = :deposit
    self.balance_amount = WalletTransaction.last_balance_amount(wallet) + credit_amount
    self.save!
  end

  def self.last_balance_amount(current_wallet)
    begin
      last_wallet_transaction(current_wallet).balance_amount
    rescue
      0.0
    end
  end

  def self.last_wallet_transaction(current_wallet)
    current_wallet.wallet_transactions.last
  end

  def wallet_ownership
    if wallet.owner_type.eql?('User')
      errors.add(:wallet_id, "not found") unless wallet.owner_id == self.current_user.id
    end

    if wallet.owner_type.eql?('Team')
      errors.add(:wallet_id, "not found") unless self.current_user.teams.pluck(:id).include? wallet.owner_id
    end
  end

  def is_deposit?
    is_deposit
  end

  def is_withdraw?
    is_withdraw
  end

  def is_transfer?
    is_transfer
  end

  def skip_validation?
    skip_validation
  end

  def withdraw
    self.source_payment = nil
    self.transaction_type = :withdraw
    new_balance_amount = calculate_debit_amount(debit_amount)

    self.balance_amount = new_balance_amount
    self.save!
  end

  def transfer
    ActiveRecord::Base.transaction do
      # Transfer
      new_debit_amount = self.transfer_amount
      new_balance_amount = calculate_debit_amount(new_debit_amount)

      self.target_payment = nil
      self.source_payment = nil
      self.transaction_type = :transfer
      self.debit_amount = new_debit_amount
      self.balance_amount = new_balance_amount
      self.save!

      # Receiver
      receiver_balance_amount = WalletTransaction.last_balance_amount(target_wallet) + new_debit_amount
      receiver_transaction = WalletTransaction.new(
        wallet_id: self.target_wallet_id,
        target_payment: nil,
        source_payment: nil,
        transaction_type: :transfer,
        credit_amount: new_debit_amount,
        balance_amount: receiver_balance_amount,
        skip_validation: true,
        source_wallet_id: self.wallet_id
      )

      receiver_transaction.save!
    end
  end

  def calculate_debit_amount(param_debit_amount)
    new_balance_amount = WalletTransaction.last_balance_amount(wallet) - param_debit_amount

    raise 'Insufficient fund' if new_balance_amount < 0

    new_balance_amount
  end

  def validate_transfer
    errors.add(:target_wallet_id, "can't be same with wallet_id") if wallet_id == target_wallet_id
  end

  def self.balance(user)
    {
      my_wallets: user.my_wallets,
      team_wallets: user.team_wallets
    }
  end
end

class Wallet < ApplicationRecord
  has_many :wallet_transactions
  belongs_to :owner, polymorphic: true

  validates :name, :owner_type, presence: true
end

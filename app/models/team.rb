class Team < ApplicationRecord
  has_and_belongs_to_many :users, join_table: :user_teams
  has_many :wallets, foreign_key: "owner_id", as: :owner
  has_many :wallet_transactions, through: :wallets, source: :wallet_transactions

  validates :name, presence: true
end

class V1::WalletsController < ApplicationController
  before_action :authorize_request

  def deposit
    begin
      @wallet_transaction = WalletTransaction.new(wallet_transaction_params.merge!(
        current_user: @current_user,
        is_deposit: true
      ))

      if @wallet_transaction.valid? && @wallet_transaction.deposit
        return render json: { data: @wallet_transaction }, status: :ok
      end

      Rails.logger.warn "Error: #{@wallet_transaction.errors.full_messages.join(', ')}"
      render json: { error: @wallet_transaction.errors.full_messages }, status: :unprocessable_entity
    rescue => e
      Rails.logger.warn "Error: #{e}"
      render json: { error: [e] }, status: :unprocessable_entity
    end
  end

  def withdraw
    begin
      @wallet_transaction = WalletTransaction.new(wallet_transaction_params.merge!(
        current_user: @current_user,
        is_withdraw: true
      ))

      if @wallet_transaction.valid? && @wallet_transaction.withdraw
        return render json: { data: @wallet_transaction }, status: :ok
      end

      Rails.logger.warn "Error: #{@wallet_transaction.errors.full_messages.join(', ')}"
      render json: { error: @wallet_transaction.errors.full_messages }, status: :unprocessable_entity
    rescue => e
      Rails.logger.warn "Error: #{e}"
      render json: { error: [e] }, status: :unprocessable_entity
    end
  end

  def transfer
    begin
      @wallet_transaction = WalletTransaction.new(wallet_transaction_params.merge!(
        current_user: @current_user,
        is_transfer: true
      ))

      if @wallet_transaction.valid? && @wallet_transaction.transfer
        return render json: { data: @wallet_transaction }, status: :ok
      end

      Rails.logger.warn "Error: #{@wallet_transaction.errors.full_messages.join(', ')}"
      render json: { error: @wallet_transaction.errors.full_messages }, status: :unprocessable_entity
    rescue => e
      Rails.logger.warn "Error: #{e}"
      render json: { error: [e] }, status: :unprocessable_entity
    end
  end

  def balance
    data = WalletTransaction.balance(@current_user)
    render json: { data: data }, status: :ok
  end

  private

  def wallet_transaction_params
    params.permit(:credit_amount, :wallet_id, :source_payment, :debit_amount, :target_payment, :target_wallet_id,
                  :transfer_amount)
  end
end
